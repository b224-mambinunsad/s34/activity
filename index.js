const express = require("express");
const app = express();
const port = 3000;
let users = [
		{
			username: "johndoe",
			password: "johndoe1234"
		},
		{
			username: "janesmith",
			password: "janesmith1234"
		}
	];


app.use(express.json());
app.use(express.urlencoded({extended:true}));


app.get("/home", (request,response) => {
	response.send("Welcome to the homepage")
});



app.get("/users", (request, response) => {
	response.send(users)
});

let message;
app.delete("/delete-user", (request,response) => {
	for(let i = 0; i < users.length; i++){
		if(request.body.username == users[i].username){
			users[i] = ""
			console.log(users)
			message = (`User ${request.body.username} has been deleted.`)
			break;
		}else{
			message = (`User ${request.body.username} cannot be found!`)
		}
	}
	response.send(message)
});


app.listen(port, () => console.log(`Server running at port ${port}`));